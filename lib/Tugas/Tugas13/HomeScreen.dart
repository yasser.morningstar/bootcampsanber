import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 22.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                IconButton(onPressed: () {}, icon: Icon(Icons.notifications)),
                IconButton(onPressed: () {}, icon: Icon(Icons.extension)),
              ],
            ),
            SizedBox(height: 14),
            Text.rich(
              TextSpan(
                  text: 'Welcome,       ',
                  style: TextStyle(
                      fontWeight: FontWeight.w700,
                      color: Colors.blue,
                      fontSize: 40),
                  children: [
                    TextSpan(
                      text: 'Hilmy',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.lightBlue,
                          fontSize: 38),
                    ),
                  ]),
            ),
            SizedBox(height: 20),
            TextField(
              decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(9),
                  borderSide: BorderSide(color: Colors.lightBlueAccent),
                ),
                prefixIcon: Icon(
                  Icons.search,
                  size: 18,
                ),
                hintText: 'Search',
              ),
            ),
            SizedBox(height: 70),
            Text(
              'Recomended Place',
              style: TextStyle(fontWeight: FontWeight.w400, fontSize: 20),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
              height: 300,
              child: GridView.count(
                padding: EdgeInsets.zero,
                crossAxisCount: 2,
                childAspectRatio: 1.400,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                physics: NeverScrollableScrollPhysics(),
                children: [
                  for (var country in countries)
                    Image.asset('assets/images/$country.png')
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

final countries = ['Berlin', 'Monas', 'Tokyo', 'Roma'];
